"""
    My favorite chart: heat map
    Author: Chen GONG (220041030)
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors


def heat_map():
    """ Draw a heat map for a factor """
    # Define 100 colors using an existing color map
    newcolors = plt.get_cmap('viridis',100).colors
    red_map = mpl.cm.get_cmap('Reds')
    newcolors[  :5, :] = red_map(0.01) 
    newcolors[5: 10, :] = red_map(0.3)
    newcolors[10: 20, :] = red_map(0.5)    
    newcolors[20:50, :] = red_map(0.7)
    newcolors[50:100, :] = red_map(0.95)
    mycmap = colors.ListedColormap(newcolors)
    data_array = np.random.randn(70)
    data_matrix = pd.DataFrame(data_array.reshape((5, 14)))
    
    fig, ax = plt.subplots(figsize=(20,4))
    sns.heatmap(
        data=data_matrix,
        ax=ax,
        vmax=1, vmin=0,
        cmap=mycmap,
        cbar_kws={ 'pad': .02,'ticks': [0, 0.05, 0.1, 0.2, 0.5,1]},
        linewidths = 0.01,
        linecolor = 'gray'
    )
    #set label
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
    #set title
    plt.text(25,8, "A Heat Map for P-value", fontsize = 20, fontstyle='italic')
    plt.title("A Heat Map for Normal Distributed Points", fontsize = 20, fontstyle='italic')
    plt.show()


if __name__ == "__main__":
    heat_map()