#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# author: Wang,Xudong 220041020 SDS time:2020-10-24

# if you not install bokeh, please install it by "pip install bokeh"
# you should run this script in jupyter notebook for dynamic plot show

import numpy as np
from bokeh.plotting import figure, output_file, show

##generate sample data
N = 4000
x = np.random.random(size=N) * 100
y = np.random.random(size=N) * 100
radii = np.random.random(size=N) * 1.5
colors = [
    "#%02x%02x%02x" % (int(r), int(g), 150) for r, g in zip(50+2*x, 30+2*y)
]

# show picture in notbook
output_notebook()

TOOLS = "crosshair,pan,wheel_zoom,box_zoom,reset,box_select,lasso_select"

# create picture
p = figure(tools=TOOLS, x_range=(0, 100), y_range=(0, 100))

# plot the dynamic points' cycle
p.circle(x, y, radius=radii, fill_color=colors, fill_alpha=0.6, line_color=None)

# show the picture
show(p)
