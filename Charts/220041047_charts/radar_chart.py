import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from math import pi

# Set data
df_data = [{'client_id': 'c1', 'coffee': 2, 'cheese': 2, 'bread':5, 'beer':4, 'beef':2},
      {'client_id': 'c2', 'coffee': 5, 'cheese': 1, 'bread':3, 'beer':3, 'beef':3},
      {'client_id': 'c3', 'coffee': 4, 'cheese': 3, 'bread':3, 'beer':2, 'beef':5},
      {'client_id': 'c4', 'coffee': 1, 'cheese': 2, 'bread':5, 'beer':4, 'beef':2},
      {'client_id': 'c5', 'coffee': 3, 'cheese': 3, 'bread':4, 'beer':5, 'beef':3}]

df = pd.DataFrame(df_data)

# number of variable
categories = list(df)[1:]

# value of data
values = df.mean().values.flatten().tolist()
print(values)
values += values[:1] # repeat the first value to close the circular graph
print(values)
# What will be the angle of each axis in the plot? 
# we divide the plot / number of variable
angles = [n / float(len(categories)) * 2 * pi for n in range(len(categories))]
angles += angles[:1]
#print(angles)

# Initialise the spider plot
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(8, 8),
                       subplot_kw=dict(polar=True))

# Draw one axe per variable + add labels labels yet
plt.xticks(angles[:-1], categories, color='grey', size=12)

# Draw ylabels
plt.yticks(np.arange(1, 6), ['1', '2', '3', '4', '5'],
           color='grey', size=12)
plt.ylim(0, 5)
ax.set_rlabel_position(30)
 
# Plot data
ax.plot(angles, values, linewidth=1, linestyle='solid')
# Fill area
ax.fill(angles, values, 'skyblue', alpha=0.4)

plt.show()