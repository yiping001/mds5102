# -*- coding: UTF-8 -*-
'''
@Project ：bitbucket 
@File    ：test_data.py
@Author  ：Yunzhong Luo 
@Date    ：2020/12/27 1:41 下午 
'''

line_example_data = [
    [[119.107078, 36.70925, 1000], [116.587245, 35.415393, 1000]],
    [[117.000923, 36.675807], [120.355173, 36.082982]],
    [[118.047648, 36.814939], [118.66471, 37.434564]],
    [[121.391382, 37.539297], [119.107078, 36.70925]],
    [[116.587245, 35.415393], [122.116394, 37.509691]],
    [[119.461208, 35.428588], [118.326443, 35.065282]],
    [[116.307428, 37.453968], [115.469381, 35.246531]],
]

scatter_example_data = [
    ("黑龙江", [127.9688, 45.368, 100]),
    ("内蒙古", [110.3467, 41.4899, 100]),
    ("吉林", [125.8154, 44.2584, 100]),
    ("辽宁", [123.1238, 42.1216, 100]),
    ("河北", [114.4995, 38.1006, 100]),
    ("天津", [117.4219, 39.4189, 100]),
    ("山西", [112.3352, 37.9413, 100]),
    ("陕西", [109.1162, 34.2004, 100]),
    ("甘肃", [103.5901, 36.3043, 100]),
    ("宁夏", [106.3586, 38.1775, 100]),
    ("青海", [101.4038, 36.8207, 100]),
    ("新疆", [87.9236, 43.5883, 100]),
    ("西藏", [91.11, 29.97, 100]),
    ("四川", [103.9526, 30.7617, 100]),
    ("重庆", [108.384366, 30.439702, 100]),
    ("山东", [117.1582, 36.8701, 100]),
    ("河南", [113.4668, 34.6234, 100]),
    ("江苏", [118.8062, 31.9208, 100]),
    ("安徽", [117.29, 32.0581, 100]),
    ("湖北", [114.3896, 30.6628, 100]),
    ("浙江", [119.5313, 29.8773, 100]),
    ("福建", [119.4543, 25.9222, 100]),
    ("江西", [116.0046, 28.6633, 100]),
    ("湖南", [113.0823, 28.2568, 100]),
    ("贵州", [106.6992, 26.7682, 100]),
    ("广西", [108.479, 23.1152, 100]),
    ("海南", [110.3893, 19.8516, 100]),
    ("上海", [121.4648, 31.2891, 100]),
]

bar_example_data = [
    ("黑龙江", [127.9688, 45.368, 100]),
    ("内蒙古", [110.3467, 41.4899, 300]),
    ("吉林", [125.8154, 44.2584, 300]),
    ("辽宁", [123.1238, 42.1216, 300]),
    ("河北", [114.4995, 38.1006, 300]),
    ("天津", [117.4219, 39.4189, 300]),
    ("山西", [112.3352, 37.9413, 300]),
    ("陕西", [109.1162, 34.2004, 300]),
    ("甘肃", [103.5901, 36.3043, 300]),
    ("宁夏", [106.3586, 38.1775, 300]),
    ("青海", [101.4038, 36.8207, 300]),
    ("新疆", [87.9236, 43.5883, 300]),
    ("西藏", [91.11, 29.97, 300]),
    ("四川", [103.9526, 30.7617, 300]),
    ("重庆", [108.384366, 30.439702, 300]),
    ("山东", [117.1582, 36.8701, 300]),
    ("河南", [113.4668, 34.6234, 300]),
    ("江苏", [118.8062, 31.9208, 300]),
    ("安徽", [117.29, 32.0581, 300]),
    ("湖北", [114.3896, 30.6628, 300]),
    ("浙江", [119.5313, 29.8773, 300]),
    ("福建", [119.4543, 25.9222, 300]),
    ("江西", [116.0046, 28.6633, 300]),
    ("湖南", [113.0823, 28.2568, 300]),
    ("贵州", [106.6992, 26.7682, 300]),
    ("广西", [108.479, 23.1152, 300]),
    ("海南", [110.3893, 19.8516, 300]),
    ("上海", [121.4648, 31.2891, 1300]),
]