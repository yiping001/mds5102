# -*- coding: UTF-8 -*-
'''
@Project ：bitbucket
@Author  ：Yunzhong Luo 
@Date    ：2020/12/27 12:58 下午 
'''

from pyecharts import options as opts
from pyecharts.charts import Map3D
from pyecharts.globals import ChartType
from pyecharts.commons.utils import JsCode

class Map3Draw:
    """
        This is a class for obtaining and draw the 3D Map

        Map3D class is a class designed for different type of 3D Map including scatter, plot, etc.
        You can decide to save the plot locally or just get the plot object for future use

        Attributes:
            type: The chart type you want to draw
            data: The data needed for drawing the 3D Map
            obj: The chart object
    """

    def __init__(self, **dic):
        """
        :param dic: Input required initial data
        """
        self.type = dic['type']
        self.data = dic['data']

    def getObj(self):
        """
        :return: Return the map object
        """
        try:
            return self.obj
        except:
            print('Please run the draw function to get the map object first!')

    def _scatteMap(self):
        """
        Draw the scatter map

        :return: Return scatter map object
        """
        c = (
            Map3D()
                .add_schema(
                itemstyle_opts=opts.ItemStyleOpts(
                    color="rgb(5,101,123)",
                    opacity=1,
                    border_width=0.8,
                    border_color="rgb(62,215,213)",
                ),
                map3d_label=opts.Map3DLabelOpts(
                    is_show=False,
                    formatter=JsCode("function(data){return data.name + " " + data.value[2];}"),
                ),
                emphasis_label_opts=opts.LabelOpts(
                    is_show=False,
                    color="#fff",
                    font_size=10,
                    background_color="rgba(0,23,11,0)",
                ),
                light_opts=opts.Map3DLightOpts(
                    main_color="#fff",
                    main_intensity=1.2,
                    main_shadow_quality="high",
                    is_main_shadow=False,
                    main_beta=10,
                    ambient_intensity=0.3,
                ),
            )
                .add(
                series_name="Scatter3D",
                data_pair=self.data,
                type_=ChartType.SCATTER3D,
                bar_size=1,
                shading="lambert",
                label_opts=opts.LabelOpts(
                    is_show=False,
                    formatter=JsCode("function(data){return data.name + ' ' + data.value[2];}"),
                ),
            )
        )
        return c

    def _lineMap(self):
        """
        Draw the line map

        :return: Return line map object
        """
        c = (
            Map3D()
                .add_schema(
                maptype="山东",
                itemstyle_opts=opts.ItemStyleOpts(
                    color="rgb(5,101,123)",
                    opacity=1,
                    border_width=0.8,
                    border_color="rgb(62,215,213)",
                ),
                light_opts=opts.Map3DLightOpts(
                    main_color="#fff",
                    main_intensity=1.2,
                    is_main_shadow=False,
                    main_alpha=55,
                    main_beta=10,
                    ambient_intensity=0.3,
                ),
                view_control_opts=opts.Map3DViewControlOpts(center=[-10, 0, 10]),
                post_effect_opts=opts.Map3DPostEffectOpts(is_enable=False),
            )
                .add(
                series_name="",
                data_pair=self.data,
                type_=ChartType.LINES3D,
                effect=opts.Lines3DEffectOpts(
                    is_show=True,
                    period=4,
                    trail_width=3,
                    trail_length=0.5,
                    trail_color="#f00",
                    trail_opacity=1,
                ),
                linestyle_opts=opts.LineStyleOpts(is_show=False, color="#fff", opacity=0),
            )
        )
        return c

    def _barMap(self):
        """
        Draw the bar map

        :return: Return bar map object
        """
        c = (
            Map3D()
                .add_schema(
                itemstyle_opts=opts.ItemStyleOpts(
                    color="rgb(5,101,123)",
                    opacity=1,
                    border_width=0.8,
                    border_color="rgb(62,215,213)",
                ),
                map3d_label=opts.Map3DLabelOpts(
                    is_show=False,
                    formatter=JsCode("function(data){return data.name + " " + data.value[2];}"),
                ),
                emphasis_label_opts=opts.LabelOpts(
                    is_show=False,
                    color="#fff",
                    font_size=10,
                    background_color="rgba(0,23,11,0)",
                ),
                light_opts=opts.Map3DLightOpts(
                    main_color="#fff",
                    main_intensity=1.2,
                    main_shadow_quality="high",
                    is_main_shadow=False,
                    main_beta=10,
                    ambient_intensity=0.3,
                ),
            )
                .add(
                series_name="bar3D",
                data_pair=self.data,
                type_=ChartType.BAR3D,
                bar_size=1,
                shading="lambert",
                label_opts=opts.LabelOpts(
                    is_show=False,
                    formatter=JsCode("function(data){return data.name + ' ' + data.value[2];}"),
                ),
            )
        )
        return c

    def draw(self, save=False, title=False):
        """
        Draw the selected 3D Map

        :param save: Enter the location to be saved. False by default.
        :param title: Enter the title for chart. False by default.
        :return: None
        """
        if self.type == 'scatter':
            self.obj = self._scatteMap()
        elif self.type == 'line':
            self.obj = self._lineMap()
        elif self.type == 'bar':
            self.obj = self._barMap()
        else:
            print("Please Make Sure the Type in 'scatter', 'line', 'bar'")
        if title:
            self.obj.set_global_opts(title_opts=opts.TitleOpts(title=title))
        if save:
            self.obj.render(save)



