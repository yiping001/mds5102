#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
@Project ：bitbucket 
@File    ：main.py
@Author  ：Yunzhong Luo 
@Date    ：2020/12/27 1:44 下午 
'''
from map3D_chart import *
from test_data import *

if __name__ == "__main__":
    map = Map3Draw(type='line', data=line_example_data)
    map.draw('./test1.html', 'Line')
    map = Map3Draw(type='bar', data=line_example_data)
    map.draw('./test2.html', 'Bar')
    map = Map3Draw(type='scatter', data=line_example_data)
    map.draw('./test3.html', 'Scatter')

