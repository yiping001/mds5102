## WOE categorical variables
def calWOE(df, var, target):
    eps = 0.00001
    gbi = pd.crosstab(df[var],df[target])+eps
    gb = df[target].value_counts() + eps
    gbri = gbi/gb
    gbri['woe'] = np.log(gbri[1]/gbri[0])
    return gbri['woe'].to_dict()

# woeMap = calWOE(feifaAll,'industryphy','label')
# feifaAll['indstryphyNew'] = feifaAll['industryphy'].map(woeMap)

def logit_transform(data):
    return np.log((data + 1e-10).divide(1 - (data + 1e-10)))

def ZscoreNormalization(x):    
    """Z-score normaliaztion"""
    x = (x - np.mean(x)) / np.std(x)
    return x
	
# use psm	
def get_matching_pairs(treated_df, non_treated_df, col_i, scaler=True):
    treated_x = treated_df[col_i]
    non_treated_x = non_treated_df[col_i]
    
    if scaler == True:
        scaler = StandardScaler()
    if scaler:
        scaler.fit(treated_x)
        treated_x = scaler.transform(treated_x)
        non_treated_x = scaler.transform(non_treated_x)

    nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(non_treated_x)
    distances, indices = nbrs.kneighbors(treated_x)
    indices = indices.reshape(indices.shape[0])
    
    matched = non_treated_df.iloc[indices]
    return matched
	

# 精确率、召回率、F1、准确率
def p_r_f1_a(acts,pres):
    TP, FP, TN, FN = 0, 0, 0, 0
    for i in range(len(acts)):
        if acts[i] == 1 and pres[i] == 1:
            TP += 1
        if acts[i] == 0 and pres[i] == 1:
            FP += 1
        if acts[i] == 1 and pres[i] == 0:
            FN += 1
        if acts[i] == 0 and pres[i] == 0:
            TN += 1    
    # 精确率Precision
    P = TP / (TP+FP)
    # 召回率Recall
    R = TP / (TP+FN)  
    # F1
    F1 = 2 / (1/P + 1/R)
    # 准确率Accuracy
    A = (TP+TN) / (TP+FP+FN+TN) 
    print('Precision: {0:.3f}, Recall: {1:.3f}, F1:{2:.3f}, Accuracy:{3:.3f}'.format(P, R, F1, A))   
     # return P, R, F1, A


