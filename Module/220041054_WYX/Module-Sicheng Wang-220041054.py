#!/usr/bin/env python
# coding: utf-8


## It is my module to summrize the function

# Calculate VWAP of security
def wavg(dataFrame, variable_column, weighting_column):
    price = dataFrame[variable_column]
    weight = dataFrame[weighting_column]
    try:
        return (price * weight).sum() / weight.sum()
    except ZeroDivisionError:
        return price.mean()

    
# Find the largest and the smallest number in list
def find_extreme(List):
    temp_largest = None
    temp_smallest = None
    for number in List:
        if temp_largest is None or number > temp_largest:
            temp_largest = number
        if temp_smallest is None or number < temp_smallest:
            temp_smallest = number
    return {'The largest number': temp_largest, 
            'The smallest number': temp_smallest}


# Count frequency of words in a list:
def count_freq(List):
    count = dict()
    for item in List:
        count[item] = count.get(item, 0) + 1
    return count


# Count the most frquent word in a flie and its frequency in a file 
def most_freq_word(name):
    file = open(name)
    counts = dict()
    for line in file:
        words = line.split()
        for word in words:
            counts[word] = counts.get(word,0) + 1

    bigcount = None
    bigword = None
    for word, count in counts.items():
        if bigcount is None or count > bigcount:
            bigword = word
            bigcount = count
    return (bigword, bigcount)



# Find the Top n common words
def top_words(name, n):
    file = open(name)
    counts = dict()
    for line in file:
        words = line.split()
        for word in words:
            counts[word] = counts.get(word,0) + 1
    
    List = list()
    for key, val in counts.items():
        List.append((val, key))
    List = sorted(List, reverse=True)
    for val, key in List[:n]:
        print(key, val)
            


# User-defined function to load yahoo finance historical data pages and process the data
import requests
import pandas as pd
yahoo_finance_url = 'https://au.finance.yahoo.com/quote/%s/history?period1=1546261200&period2=1559829600&interval=1d&filter=history&frequency=1d'

def fetch_Yahoo_Finance(security): 
    yahoo_hist_price_page = requests.get(yahoo_finance_url % security)
    if yahoo_hist_price_page.status_code == 200:
        print('Successful Fetching:', security)
        for column in yahoo_hist_price_DataFrame.columns:
            if column != 'Date':
                yahoo_hist_price_DataFrame[column] = pd.to_numeric(yahoo_hist_price_DataFrame[column], errors='coerce')
        return yahoo_hist_price_DataFrame
    else:
        raise Exception('Some Error Happened. Http Error Code %d' % yahoo_hist_price_page.status_code)

        

        
# A cat class
class Cat:
    def __init__(self, name, age):  
        self.name = name
        self.age = age
    
    def meow(self):
        print("meow meow!")
    
    def catinfo(self):
        print(self.name + " is " + str(self.age) + " year(s) old.")


        
# Residual Calculation
def calculate_residuals(model, features, label):
    """
    Creates predictions on the features with the model and calculates residuals
    """
    predictions = model.predict(features)
    df_results = pd.DataFrame({'Actual': label, 'Predicted': predictions})
    df_results['Residuals'] = abs(df_results['Actual']) - abs(df_results['Predicted'])
    return df_results



#Linearity Testing
def linear_assumption(model, features, label):
    from sklearn.linear_model import LinearRegression
    print('Assumption 1: Linear Relationship between the Target and the Feature', '\n')
    
    # Calculating residuals for the plot
    df_results = calculate_residuals(model, features, label)
    
    # Plotting the actual vs predicted values
    sns.lmplot(x='Actual', y='Predicted', data=df_results, fit_reg=False, height=7)
        
    # Plotting the diagonal line
    line_coords = np.arange(df_results.min().min(), df_results.max().max())
    plt.plot(line_coords, line_coords,  # X and y points
             color='darkorange', linestyle='--')
    plt.title('Actual vs. Predicted')
    plt.show()
    


#Normality of the Error Terms
def normal_errors_assumption(model, features, label, p_value_thresh=0.05):
    from statsmodels.stats.diagnostic import normal_ad
    print('Assumption 2: The error terms are normally distributed', '\n')
    
    # Calculating residuals for the Anderson-Darling test
    df_results = calculate_residuals(model, features, label)
    
    print('Using the Anderson-Darling test for normal distribution')

    # Performing the test on the residuals
    p_value = normal_ad(df_results['Residuals'])[1]
    print('p-value from the test - below 0.05 generally means non-normal:', p_value)
    
    # Reporting the normality of the residuals
    if p_value < p_value_thresh:
        print('Residuals are not normally distributed')
    else:
        print('Residuals are normally distributed')
    
    # Plotting the residuals distribution
    plt.subplots(figsize=(12, 6))
    plt.title('Distribution of Residuals')
    sns.distplot(df_results['Residuals'])
    plt.show()
    
    print()
    if p_value > p_value_thresh:
        print('Assumption satisfied')
    else:
        print('Assumption not satisfied')
        print()
        print('Confidence intervals will likely be affected')
        print('Try performing nonlinear transformations on variables')

        
        
#No Multicollinearity among Predictors
def multicollinearity_assumption(model, features, label, feature_names=None):
    
    from statsmodels.stats.outliers_influence import variance_inflation_factor
    print('Assumption 3: Little to no multicollinearity among predictors')
        
    # Plotting the heatmap
    plt.figure(figsize = (10,8))
    sns.heatmap(pd.DataFrame(features, columns=feature_names).corr(), annot=True)
    plt.title('Correlation of Variables')
    plt.show()
        
    print('Variance Inflation Factors (VIF)')
    print('> 10: An indication that multicollinearity may be present')
    print('> 100: Certain multicollinearity among the variables')
    print('-------------------------------------')
       
    # Gathering the VIF for each variable
    VIF = [variance_inflation_factor(features, i) for i in range(features.shape[1])]
    for idx, vif in enumerate(VIF):
        print('{0}: {1}'.format(feature_names[idx], vif))
        
    # Gathering and printing total cases of possible or definite multicollinearity
    possible_multicollinearity = sum([1 for vif in VIF if vif > 10])
    definite_multicollinearity = sum([1 for vif in VIF if vif > 100])
    print()
    print('{0} cases of possible multicollinearity'.format(possible_multicollinearity))
    print('{0} cases of definite multicollinearity'.format(definite_multicollinearity))
    print()

    if definite_multicollinearity == 0:
        if possible_multicollinearity == 0:
            print('Assumption satisfied')
        else:
            print('Assumption possibly satisfied')
            print()
            print('Coefficient interpretability may be problematic')
            print('Consider removing variables with a high Variance Inflation Factor (VIF)')

    else:
        print('Assumption not satisfied')
        print()
        print('Coefficient interpretability will be problematic')
        print('Consider removing variables with a high Variance Inflation Factor (VIF)')
        
        

        
#No Autocorrelation of the Error Terms
def autocorrelation_assumption(model, features, label):
    from statsmodels.stats.stattools import durbin_watson
    print('Assumption 4: No Autocorrelation', '\n')
    
    # Calculating residuals for the Durbin Watson-tests
    df_results = calculate_residuals(model, features, label)

    print('\nPerforming Durbin-Watson Test')
    print('Values of 1.5 < d < 2.5 generally show that there is no autocorrelation in the data')
    print('0 to 2< is positive autocorrelation')
    print('>2 to 4 is negative autocorrelation')
    print('-------------------------------------')
    durbinWatson = durbin_watson(df_results['Residuals'])
    print('Durbin-Watson:', durbinWatson)
    if durbinWatson < 1.5:
        print('Signs of positive autocorrelation', '\n')
        print('Assumption not satisfied')
    elif durbinWatson > 2.5:
        print('Signs of negative autocorrelation', '\n')
        print('Assumption not satisfied')
    else:
        print('Little to no autocorrelation', '\n')
        print('Assumption satisfied')
        
        
        
#Homoscedasticity
def homoscedasticity_assumption(model, features, label):
    print('Assumption 5: Homoscedasticity of Error Terms', '\n')
    
    print('Residuals should have relative constant variance')
        
    # Calculating residuals for the plot
    df_results = calculate_residuals(model, features, label)

    # Plotting the residuals
    plt.subplots(figsize=(12, 6))
    ax = plt.subplot(111)  # To remove spines
    plt.scatter(x=df_results.index, y=df_results.Residuals, alpha=0.5)
    plt.plot(np.repeat(0, df_results.index.max()), color='darkorange', linestyle='--')
    ax.spines['right'].set_visible(False)  # Removing the right spine
    ax.spines['top'].set_visible(False)  # Removing the top spine
    plt.title('Residuals')
    plt.show()  