# 爬取讲话内容，爬取失败进行人工搜索

import requests
import re

def getHtml(url):
	response = requests.get(url)
	content = response.text.encode('iso-8859-1').decode('utf-8',errors='ignore')
	return content

file = open('rawdata.csv')

for line in file:
	try:
		row = line.strip('\n').split(',')
		url = row[0]
		page = getHtml(url)
		text = re.findall('<!--enpcontent--><P>.*</P><!--/enpcontent-->',page)[0]
		newfile = open('../data/'+row[2]+'--'+row[1]+'.txt','w')
		newfile.write(text)
		newfile.close()
	except:
		print(line)

file.close()