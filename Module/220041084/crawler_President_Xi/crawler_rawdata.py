# 获取所有讲话网址、题目、时间

import requests
import re

def getHtml(url):
	try:
		response = requests.get(url)
		content = response.text.encode('iso-8859-1').decode('GBK',errors='ignore')
		return content
	except:
		return "0"

rawlist = []

for i in range(1,11):
	url = 'http://cpc.people.com.cn/xuexi/GB/387488/index' + str(i) + '.html'
	page = getHtml(url)
	ret = re.findall('<div class="con"><h3>.*<\/div>',page)
	for each in ret:
		rawlist.append(each)

datalist = set(rawlist)
file = open('rawdata.csv','w')

for each in datalist:
	try:
		url = re.findall('[a-zA-z]+://[^\s]*',each)[0]
		title = re.findall('target="_blank">.*</a></h3><em>',each)[0]
		time = re.findall('\（.*</em>',each)[0]
		file.write(url+','+title+','+time+'\n')
	except:
		print(each)

file.close()