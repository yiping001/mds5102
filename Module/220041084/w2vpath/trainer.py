import torch
import torch.optim as optim
from torch.utils.data import DataLoader
from tqdm import tqdm

from data_reader import DataReader, Word2vecDataset
from model import SkipGramModel
import sys
#https://github.com/Andras7/word2vec-pytorch

"""
input parameters of this code:
trainer.py :
  input_file
  output_dir (the dir in which w2i, i2w, embeddings are saved
  min_count  (暂时没用
data_reader.py :
  en_core_web_sm
"""

class Word2VecTrainer:
    def __init__(self, input_file, output_dir, emb_dimension=300, batch_size=32, window_size=2, iterations=3,
                 initial_lr=0.001, min_count=5):

        self.data = DataReader(input_file, min_count)
        dataset = Word2vecDataset(self.data, window_size)
        self.dataloader = DataLoader(dataset, batch_size=batch_size,
                                     shuffle=True, num_workers=4, collate_fn=dataset.collate)

        self.output_dir = output_dir
        self.emb_size = len(self.data.word2id)
        self.emb_dimension = emb_dimension
        self.batch_size = batch_size
        self.iterations = iterations
        self.initial_lr = initial_lr
        self.skip_gram_model = SkipGramModel(self.emb_size, self.emb_dimension)

        self.use_cuda = torch.cuda.is_available()
        self.device = torch.device("cuda:0" if self.use_cuda else "cpu")
        if self.use_cuda:
            self.skip_gram_model.cuda()

    def train(self):

        for iteration in range(self.iterations):

            print("\n\n\nIteration: " + str(iteration + 1))
            p = list(self.skip_gram_model.parameters()) #这是一个iterable, SparseAdam遍历了一遍，optimizier又遍历了一遍耗尽了
            optimizer = optim.SparseAdam(p, lr=self.initial_lr)
            lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, len(self.dataloader))

            running_loss = 0.0
            for i, sample_batched in enumerate(tqdm(self.dataloader)):
                if len(sample_batched[0]) > 1:
                    pos_u = sample_batched[0].to(self.device)
                    pos_v = sample_batched[1].to(self.device)
                    neg_v = sample_batched[2].to(self.device)

                    optimizer.zero_grad()
                    loss = self.skip_gram_model.forward(pos_u, pos_v, neg_v)
                    loss.backward()
                    optimizer.step()
                    lr_scheduler.step()

                    running_loss = running_loss * 0.9 + loss.item() * 0.1
                    if i > 0 and i % 500 == 0:
                        print(" Loss: " + str(running_loss))

            self.skip_gram_model.save_embedding(self.data.id2word, self.data.word2id, self.output_dir)


if __name__ == '__main__':
    # /data/cc/embedding/word2vec-deps-master/alice_in_wonderland.txt
    # anscom_mobile-phones  anscom_mobile_sm
    # /data/cc/data/stackexchange/bitcoin/qtitles  se_bitcoin/
    # cooking academia
    w2v = Word2VecTrainer(input_file="/data/cc/embedding/word2vec-deps-master/anscom_mobile_sm", output_dir='t1/')
    #dom=sys.argv[1]
    #w2v = Word2VecTrainer(input_file=f"/data/cc/data/stackexchange/{dom}/qtitles", output_dir=f'se_{dom}/')
    # w2v = Word2VecTrainer(input_file="/data/cc/embedding/word2vec-deps-master/alice_in_wonderland.txt", output_dir='alice/')
    w2v.train()
