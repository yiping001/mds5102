import pickle
import  torch

# DIR='/data/cc/pycharm/MFAE/0intent/w2vpth/enphoneall/'
# DIR='/data/cc/pycharm/MFAE/0intent/w2vpth/se_bitcoin/'
# DIR='/data/cc/pycharm/MFAE/0intent/w2vpth/se_cooking/'
DIR='/data/lc/w2vpth/t1/'
embedding= torch.load(DIR+'emb.pt')#ndarray  word_index-> embedding
i2w=pickle.load(open(DIR+'id2word.pkl', 'rb'))
w2i=pickle.load(open(DIR+'word2id.pkl', 'rb'))
topn=10

def print_top_words(input_word):
    print(f'input_word: --{input_word}---')
    if input_word not in w2i:
        print('OOV. ')
        return
    ini=w2i[input_word]
    target_embedding = embedding[ini]
    sims = torch.nn.functional.cosine_similarity(target_embedding.view(1, -1), embedding)
    sorted_id = torch.argsort(sims, descending=True)
    for j in sorted_id[1: topn + 1]:
        j=j.item()
        print(i2w[j].strip() + "\t" + str(sims[j].item()))
    print()

f=print_top_words

#ws='alice looked fell look me nice open understand grass'  # Alice
# ws= 'secure Satoshi block economy money bitcoin Cryptocurrency mining profitable' # bitcoin
# ws= 'egg bacon chicken oven tomato bread rice spices water mushroom oil' # cooking
ws= 'Switzerland postdoc PhD conference thesis Supervisor funding reviewer jobs' # academia
for w in ws.split(' '):
    print_top_words(w.lower())
