from collections import defaultdict

import numpy as np
import spacy
import torch
from torch.utils.data import Dataset

np.random.seed(12345)


class DataReader:
    NEGATIVE_TABLE_SIZE = 1e8

    def __init__(self, inputFileName, min_count):

        self.negatives = []
        self.discards = []  # dep embedding暂时不用这个功能
        self.pos_train = []  # list of positive training pairs
        self.negpos = 0
        # index all depndency triples by their head, so we can sort them in different ways
        # self.indexed_training = defaultdict(list)
        self.word2id = dict()
        self.id2word = dict()
        self.sentences_count = 0
        self.token_count = 0
        self.word_frequency = dict()  # word => int(freq)

        self.inputFileName = inputFileName
        self.read_words(min_count)
        self.initTableNegatives()
        self.initTableDiscards()

    def _good_dep_relation(self, doc, examples):
        # we don't really care about training on all deps
        bad_deps = ['ROOT', 'compound', 'pobj', 'punct', '', 'case']
        # examples 存储好的 tuple: (head,  deplbl, dep)
        # training pairs:
        #    head -> lbl_dependent
        #    dependent -> -lbl_head
        for sent in doc.sents:
            for word in sent:
                # casing should probably be a paramater
                head = word.head.text.lower()
                dep = word.text.lower()
                deplbl = word.dep_  # 是针对 head -> dep的提出的
                if deplbl in bad_deps: continue
                # If we see a prepositional dependency, we want to merge it
                # so ('scientist', prep, 'with') and ('with', pobj,'telescope)
                # becomes ('scientist, 'prep_with', 'telescope)
                if deplbl == 'prep':
                    for c2 in word.children:
                        if (c2.dep_ == 'pobj'):
                            examples.append((head, "prep_" + dep, c2.text.lower()))
                else:
                    examples.append((head, deplbl, dep))


    def read_words(self, min_count):
        """
        init self.sentences_count, self.token_count, self.word2id, self.id2word, self.word_frequency
        :param min_count:
        :return:
        """

        print('loading spacy model...')
        nlp = spacy.load('en_core_web_md')  # 小的快速测试
        print('spacy model loaded...')

        print('parsing doc...')
        # incrementally parse due to spacy parsing limit， add to examples
        examples = []
        with open(self.inputFileName, 'r')as ifile:
            N = 500000
            s = ''
            n=0
            for line in ifile:
                n+=1
                s += line
                if len(s) >= N:
                    doc = nlp(s)
                    s = ''
                    self._good_dep_relation(doc, examples)
                    print(f'parsed {n} lines.')
            doc = nlp(s)
            self._good_dep_relation(doc, examples)

            # s = """Tom killed his teacher with a knife and punch.
            # Australian scientist discovers star with a telescope."""
        print('doc parsed...')

        word_frequency = dict()
        for (head, lbl, dep) in examples:  # head, lbl, dep
            forward_l = head
            forward_r = f'{lbl}_{dep}'
            backward_l = dep
            backward_r = f'-{lbl}_{head}'
            for word in [forward_l, forward_r, backward_l, backward_r]:
                word_frequency[word] = word_frequency.get(word, 0) + 1

        wid = 0
        for w, c in word_frequency.items():
            if c < min_count: continue
            self.token_count += 1
            self.word2id[w] = wid
            self.id2word[wid] = w
            self.word_frequency[wid] = c
            wid += 1
        print("Total embeddings: " + str(len(self.word2id)))

        for (head, lbl, dep) in examples:  # head, lbl, dep
            forward_l = head
            forward_r = f'{lbl}_{dep}'
            backward_l = dep
            backward_r = f'-{lbl}_{head}'
            if forward_l not in self.word2id or forward_r not in self.word2id  \
                or   backward_l not in self.word2id or   backward_r not in self.word2id:
                continue
            self.pos_train.append([self.word2id[forward_l], self.word2id[forward_r]])
            self.pos_train.append([self.word2id[backward_l], self.word2id[backward_r]])

    # for sub-sampling frequent words
    def initTableDiscards(self):
        t = 0.0001  # Sampling rate，见某个 生硬 的 公式 http://mccormickml.com/2017/01/11/word2vec-tutorial-part-2-negative-sampling/
        f = np.array(list(self.word_frequency.values())) / self.token_count
        # probability of keeping a word
        # discards[word2id[w]]
        self.discards = np.sqrt(t / f) + (t / f)

    # The paper says that selecting 5-20 words works well for smaller datasets,
    # and you can get away with only 2-5 words for large datasets.
    # TODO: 负例 目前是context和word混在一起 是否只需包含context?
    def initTableNegatives(self):
        pow_frequency = np.array(list(self.word_frequency.values())) ** 0.5
        words_pow = sum(pow_frequency)
        ratio = pow_frequency / words_pow
        count = np.round(ratio * DataReader.NEGATIVE_TABLE_SIZE)
        for wid, c in enumerate(count):
            self.negatives += [wid] * int(c)
        self.negatives = np.array(self.negatives)
        np.random.shuffle(self.negatives)

    def getNegatives(self, target, size):  # TODO check equality with target
        response = self.negatives[self.negpos:self.negpos + size]
        self.negpos = (self.negpos + size) % len(self.negatives)
        if len(response) != size:
            return np.concatenate((response, self.negatives[0:self.negpos]))
        return response


# -----------------------------------------------------------------------------------------------------------------

class Word2vecDataset(Dataset):
    def __init__(self, data: DataReader, window_size):
        self.data = data
        self.window_size = window_size
        self.input_file = open(data.inputFileName, 'r', encoding="utf8")

    def __len__(self):
        return len(self.data.pos_train)

    # return 一个正例 15个负例
    def __getitem__(self, idx):
        # print(idx)
        pos_train = self.data.pos_train
        u, v = pos_train[idx][0], pos_train[idx][1]
        ret = [u, v, self.data.getNegatives(v, 15)]
        return ret

    @staticmethod
    def collate(batch):
        all_u = [ex[0] for ex in batch]
        all_v = [ex[1] for ex in batch]
        all_neg_v = [ex[2] for ex in batch]

        return torch.LongTensor(all_u), torch.LongTensor(all_v), torch.LongTensor(all_neg_v)
