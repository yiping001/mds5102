The one .py file is python module, which you can call for use.
The one .ipynb file (decisiontree) is an example that uses functions defined in the .py file.
There is another ipynb file, which is bubble chart.
In summary, there is 1 .py file and 2 .ipynb files. Please feel free to play with them!