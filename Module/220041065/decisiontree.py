# -*- coding: utf-8 -*-
"""
decision tree spliting nodes
"""

def toNumber(df, vars):
    DF=df.copy()
    for col in df:
        if col in vars:
            cc=pd.Categorical(df[col])
            DF[col]=cc.codes+1
    return DF

def splitNodes(df, attrib):
    print("----- split by ",attrib, "----")
    values=list(set(df[attrib]))
    outDFs=[]
    for v in values:
        print(v)
        subdf=df[df[attrib]==v]
        print(subdf)
        outDFs.append(subdf)
    return list(values), outDFs

def entropy(df, attrib):
    import math
    col=list(df[attrib])
    values=set(col)
    ntotal=df.shape[0]
    ent=0.0
    for v in values:
        ratio=float(col.count(v)/ntotal)
        ent+=ratio*math.log(ratio,2)
    return -ent

def entropyIfSplitBy(df,attrib):
    values, nodes=splitNodes(df, attrib, toPrint=False)
    ntotal=df.shape[0]
    ent=0.0
    for i in range(len(values)):
        nodei=nodes[i]
        ent+=float(nodei.shape[0]/ntotal)*entropy(nodei,outcomeVar)
    return ent


