#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# author: Wang,Xudong 220041020 SDS time:2020/11/20

## Preparation and define the function:
import numpy as np
import pandas as pd
import math
import time
import matplotlib.pyplot as plt
import random
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

## Backtracking method
def backtracking_line_search(func, x, grad, sigma, gama, maximum_iterations=100000):
    s = 1
    iterations = 0
    while True:
        lvalue = func(x - s*grad )
        rvalue = func(x) + gama*s*grad.T.dot(grad)
        if lvalue <= rvalue:
            break
        else:
            s = sigma * s
        iterations += 1
        if iterations >= maximum_iterations:
            break
    return s
    
def backtracking(func, x, grad, bctk_para, maximum_iterations, tol):
    values_iterations = [func(x)]
    grad_iterations = [grad]
    x_iterations = [x]
    iteration = 0
    s, sigma, gama = bctk_para
    t_s = time.time()
    while True:
        stepsize = backtracking_line_search(func, x, grad, sigma, gama, maximum_iterations=100000)
        x = x - stepsize * grad
        grad = get_gradient(func1, func2, x)
        value = func(x)
        values_iterations.append(value)
        grad_iterations.append(grad)
        x_iterations.append(x)
        iteration = iteration + 1
        if math.sqrt(grad[0] ** 2 + grad[1] ** 2) <= tol or iteration > maximum_iterations:
            break
    t_e = time.time()
    tim = str(1000 * (t_e - t_s)) + 'ms'
    x_optimal = x_iterations[-1]
    return x_optimal, x_iterations, grad_iterations, values_iterations, tim, iteration

##Diminishing stepsize
def alpha(k):
    a_k = 0.01/math.log(k+2)
    return a_k
def diminishing_stepsize(func, x, grad, stepsize, maximum_iterations, tol):
    values_iterations = [func(x)]
    grad_iterations = [grad]
    x_iterations = [x]
    i = 0
    t_s = time.time()
    while True:
        value = func(x)
        x = x - grad * stepsize
        i = i + 1
        grad = get_gradient(func1, func2, x)
        stepsize = alpha(i)
        values_iterations.append(value)
        grad_iterations.append(grad)
        x_iterations.append(x)
        if math.sqrt(grad[0] ** 2 + grad[1] ** 2) <= tol or i > maximum_iterations:
            break
    t_e = time.time()
    tim = str(1000 * (t_e - t_s)) + 'ms'
    x_optimal = x_iterations[-1]
    return x_optimal, x_iterations, grad_iterations, values_iterations, tim, i

##Exact line search
def objective_stepsize_func(func,x,a,descent):
    value = func(x+a*descent)
    return value
def exact_line_search(func, descent, x, xl, xr, eps_2, phi):
    i = 0
    xl_ = x + xl * descent
    xr_ = x + xr * descent
    dist = math.sqrt((xl_[0] - xr_[0]) ** 2 + (xl_[1] - xr_[1]) ** 2)
    while dist >= eps_2:
        func_value_xl = objective_stepsize_func(func, x, xl, descent)
        func_value_xr = objective_stepsize_func(func, x, xr, descent)
        if func_value_xl < func_value_xr:
            xr = (1 - phi) * xr + phi * xl
        if func_value_xl >= func_value_xr:
            xl = (1 - phi) * xl + phi * xr
        i = i + 1
        xl_ = x + xl * descent
        xr_ = x + xr * descent
        dist = math.sqrt((xl_[0] - xr_[0]) ** 2 + (xl_[1] - xr_[1]) ** 2)

        if i >= 100:
            break
    return (xl + xr) / 2
    
def exact_line_search_gd(func, grad, x, eps_1, eps_2, phi, maximum_iterations):
    values_iterations = [func(x)]
    grad_iterations = [grad]
    x_iterations = [x]
    iteration = 0
    descent = -grad
    xl = 0
    xr = 2
    t_s = time.time()
    while True:
        stepsize = exact_line_search(func, descent, x, xl, xr, eps_2, phi)
        x = x + stepsize * descent
        grad = get_gradient(func1, func2, x)
        if np.linalg.norm(grad, ord=1) > 1:
            descent = -grad / np.linalg.norm(grad)
        else:
            descent = -grad
        value = func(x)
        values_iterations.append(value)
        grad_iterations.append(grad)
        x_iterations.append(x)
        iteration = iteration + 1
        if np.linalg.norm(grad, ord=1) <= eps_1:
            break
        if iteration + 1 >= maximum_iterations:
            break
    t_e = time.time()
    tim = str(1000 * (t_e - t_s)) + 'ms'
    x_optimal = x_iterations[-1]
    return x_optimal, x_iterations, grad_iterations, values_iterations, tim, iteration
    
def func1(x):
    func_value = 3+x[0]+((1-x[1])*x[1]-2)*x[1]
    return func_value

def func2(x):
    func_value = 3+x[0]+(x[1]-3)*x[1]
    return func_value

def func(x):
    func_value = func1(x)**2 + func2(x)**2
    return func_value

def get_gradient(func1,func2, x):
    gradient_func1 = 2*func1(x) + 2*func2(x)
    gradient_func2 = (2*x[1]- 3*x[1]**2 -2)*2*func1(x) + (2*x[1] - 3)*2*func2(x)
    gradient = np.array([gradient_func1,gradient_func2])
    return gradient

# visulizaion
def get_visual(method_it):

    x1 = np.linspace(-10,10,100)
    x2 = np.linspace(-2,3,100)
    x1,x2 = np.meshgrid(x1,x2)
    y = (3+x1+((1-x2)*x2-2)*x2)**2 + (3+x1+(x2-3)*x2)**2

    # formulate the contour plot of function
    fig = plt.figure()
    cset = plt.contourf(x1,x2,y,100,cmap = cm.rainbow)
    contour = plt.contour(x1,x2,y,colors = 'k')
    plt.clabel(contour,inline = True,fontsize=10)
    plt.scatter(-1,1)
    plt.text(-1.1,1.1,'(-1,1)')
    plt.scatter(-7,-1)
    plt.text(-7.1,-1.1,'(-7,-1)')
    plt.scatter(-3,0)
    plt.text(-3.1,0.1,'(-3,0)')

    #generate the iterations
    # plot the path of iterations
    for i in range(len(method_it)):
        p0 = []
        p1 = []
        s = i
        for i in range(len(method_it[s])):
            p = list(method_it[s][i])
            p0.append(p[0])
            p1.append(p[1])
        plt.plot(p0,p1,marker=',')
    plt.show()

if __name__ == "__main__":
    #Backtracking method
    x0 = [0,0]
    grad = get_gradient(func1,func2,x0)
    tol = 1e-05
    maximum_iterations = 100000
    bctk_para = (1,0.5,0.1)
    s,sigma,gama = bctk_para
    x_optimal,x_iterations,grad_iterations,values_iterations,tim,i = backtracking(func, x0, grad, bctk_para, maximum_iterations, tol)
    print('the optimal solution is ',x_optimal)
    print('the time is ', tim)
    print('the iterations ', i)
    bk_f_iterdiff = [math.log(abs(i - func(x_optimal)))for i in values_iterations[:-1]]
    bk_grad_iter = [math.log(math.sqrt(grad[0]**2+grad[1]**2)) for grad in grad_iterations[:-1]]
    bk_x_iterdiff = [math.log(math.sqrt((x-x_optimal)[0]**2+(x-x_optimal)[1]**2)) for x in x_iterations[:-1]]
    plt.style.use('ggplot')
    plt.plot(bk_grad_iter)
    plt.title('Backtracking: Norm of Gradient')
    plt.xlabel('iterations')
    plt.ylabel('log of norm of gradient')
    plt.show()
    plt.style.use('ggplot')
    plt.plot(bk_x_iterdiff)
    plt.title('Backtracking: ||x-x*||')
    plt.xlabel('iterations')
    plt.ylabel('log ||x-x*||')
    plt.show()

    #dimishing step size
    x0 = [0,0]
    grad = get_gradient(func1,func2,x0)
    tol = 1e-05
    stepsize = alpha(0)
    maximum_iterations = 100000
    x_optimal,x_iterations,grad_iterations,values_iterations,tim,i = diminishing_stepsize(func, x0, grad, stepsize, maximum_iterations, tol)
    print('the optimal solution is ',x_optimal)
    print('the time is ', tim)
    print('the iterations ', i)
    diminishing_f_iterdiff = [math.log(abs(i - func(x_optimal)))for i in values_iterations[:-1]]
    diminishing_grad_iter = [math.log(math.sqrt(grad[0]**2+grad[1]**2)) for grad in grad_iterations[:-1]]
    diminishing_x_iterdiff = [math.log(math.sqrt((x-x_optimal)[0]**2+(x-x_optimal)[1]**2)) for x in x_iterations[:-1]]
    plt.style.use('ggplot')
    plt.plot(diminishing_grad_iter)
    plt.title('Diminishing: Norm of Gradient')
    plt.xlabel('iterations')
    plt.ylabel('log norm of gradient')
    plt.show()
    plt.style.use('ggplot')
    plt.plot(diminishing_x_iterdiff)
    plt.title('Diminishing: ||x-x*||')
    plt.xlabel('iterations')
    plt.ylabel('log ||x-x*||')
    plt.show()
    
    #Exact line search
    x0 = [0,0]
    grad = get_gradient(func1,func2,x0)
    descent = -grad/np.linalg.norm(grad)
    eps_1 = 1e-05
    eps_2 = 1e-06
    maximum_iterations = 100000
    phi = (3-math.sqrt(5))/2
    x_optimal,x_iterations,grad_iterations,values_iterations,tim,i = exact_line_search_gd(func, grad, x0, eps_1, eps_2, phi, maximum_iterations)
    print('the optimal solution is ',x_optimal)
    print('the time is ', tim)
    print('the iterations ', i)
    Exact_f_iterdiff = [math.log(abs(i - func(x_optimal)))for i in values_iterations[:-1]]
    Exact_grad_iter = [math.log(math.sqrt(grad[0]**2+grad[1]**2)) for grad in grad_iterations[:-1]]
    Exact_x_iterdiff = [math.log(math.sqrt((x-x_optimal)[0]**2+(x-x_optimal)[1]**2)) for x in x_iterations[:-1]]
    plt.style.use('ggplot')
    plt.plot(Exact_grad_iter)
    plt.title('Exact Line Search: Norm of Gradient')
    plt.xlabel('iterations')
    plt.ylabel('log norm of gradient')
    plt.show()
    plt.style.use('ggplot')
    plt.plot(Exact_x_iterdiff)
    plt.title('Exact Line Search: ||x-x*||')
    plt.xlabel('iterations')
    plt.ylabel('log ||x-x*||')
    plt.show()
    
    #Compare three method
    Exact_x_iterdiff_compare = Exact_x_iterdiff
    for i in range(len(Exact_x_iterdiff)+1,100+1):
        Exact_x_iterdiff_compare.append(pd.NA)
    Exact_grad_iter_compare = Exact_grad_iter
    for i in range(len(Exact_grad_iter)+1,100+1):
        Exact_grad_iter_compare.append(pd.NA)
    df1_dict={'diminishing stepsize':diminishing_x_iterdiff[0:100],
            'exact line search':Exact_x_iterdiff_compare[0:100],
            'backtraking line search':bk_x_iterdiff[0:100]}
    df1 = pd.DataFrame(df1_dict)
    df2_dict={'diminishing stepsize':diminishing_grad_iter[0:100],
            'exact line search':Exact_grad_iter_compare[0:100],
            'backtraking line search':bk_grad_iter[0:100]}
    df2 = pd.DataFrame(df2_dict)
    plt.style.use('ggplot')
    plt.plot(df1.iloc[:,0],label='diminishing')
    plt.plot(df1.iloc[:,1][:7],label='exact line search')
    plt.plot(df1.iloc[:,2],label='backtraking')
    plt.xlabel('iterations')
    plt.ylabel('log ||x-x*||')
    plt.title('compare three methods: ||x-x*||')
    plt.legend()
    plt.grid(True,linestyle = "--",color = 'gray' ,linewidth = '0.5',axis='both')
    plt.show()
    plt.style.use('ggplot')
    plt.plot(df2.iloc[:,0],label='diminishing')
    plt.plot(df2.iloc[:,1][:7],label='exact line search')
    plt.plot(df2.iloc[:,2],label='backtraking')
    plt.xlabel('iterations')
    plt.ylabel('log norm of gradient')
    plt.title('compare three methods: norm of gradient')
    plt.legend()
    plt.grid(True,linestyle = "--",color = 'gray' ,linewidth = '0.5',axis='both')
    plt.show()

    # 3d-plot and contour plot of f(x)
    x1 = np.linspace(-10,10,100)
    x2 = np.linspace(-2,2,100)
    x1,x2 = np.meshgrid(x1,x2)
    y = (3+x1+((1-x2)*x2-2)*x2)**2 + (3+x1+(x2-3)*x2)**2
    fig = plt.figure()
    plt.style.use('ggplot')
    ax = fig.gca(projection = '3d')
    surf1 = ax.plot_surface(x1,x2,y,cmap = cm.rainbow,
                           linewidth = 0, antialiased = False)

    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    plt.show()
    fig = plt.figure()
    plt.style.use('ggplot')
    cset = plt.contourf(x1,x2,y,100,cmap = cm.rainbow)
    contour = plt.contour(x1,x2,y,colors = 'k')
    plt.clabel(contour,inline = True,fontsize=10)
    plt.scatter(-1,1)
    plt.text(-1.1,1.1,'(-1,1)')
    plt.scatter(-7,-1)
    plt.text(-7.1,-1.1,'(-7,-1)')
    plt.scatter(-3,0)
    plt.text(-3.1,0.1,'(-3,0)')
    plt.show()
    
    # mutiple initial points
    up_length_points = [[-5,2],[0,2],[5,2]]
    down_length_points = [[-5,-2],[0,-2],[5,-2]]
    left_width_points = [[-10,-1],[-10,0],[-10,1]]
    right_width_points = [[10,-1],[10,0],[10,1]]
    initial_points = up_length_points+down_length_points+left_width_points+right_width_points
    points = initial_points
    maximum_iterations = 100000
    tol = 1e-05
    eps_1 = 1e-05
    eps_2 = 1e-06
    
    #Backtracking
    x_backtrakings = []
    backtraking_point_it = []
    backtraking_df = pd.DataFrame()
    for point in points:
        x_backtracking,x_bc,grad_iterations,values_iterations,tim,i = backtracking(func, point, grad, bctk_para, maximum_iterations, tol)
        x_backtrakings.append(x_backtracking)
        backtraking_point_it.append(x_bc)
    get_visual(backtraking_point_it)

    #Exact line search
    x_exacts = []
    exact_point_it = []
    for point in points:
        x_exact, x_ec, grad_iterations, values_iterations, tim, i = exact_line_search_gd(func, grad, point, eps_1, eps_2,
                                                                                         phi, maximum_iterations)
        x_exacts.append(x_exact)
        exact_point_it.append(x_ec)
    get_visual(exact_point_it)
    
    #dimishing step size
    def alpha(k):
        a_k = 0.01/math.log(k+10)
        return a_k
    x_diminishings = []
    x_diminishing_it = []
    for point in points:
        x_dim,x_iterations,grad_iterations,values_iterations,tim,i = diminishing_stepsize(func, point, grad, stepsize, maximum_iterations, tol)
        x_diminishings.append(x_dim)
        x_diminishing_it.append(x_iterations)
    get_visual(x_diminishing_it)
