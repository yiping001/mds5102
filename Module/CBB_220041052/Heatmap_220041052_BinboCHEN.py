import pandas as pd

p_matrix = pd.read_csv('data.csv')
p_matrix.rename(columns={'Unnamed: 0': 'Factor'}, inplace=True)
p_matrix.set_index('Factor', inplace=True)
p_matrix = p_matrix.drop(['R2', 'ADJ R2'], axis=0)


def heat_map(data0):
    import pandas as pd
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    import matplotlib.colors as colors
    import seaborn as sns
    # define 100 colors using an existing color map
    newcolors = plt.get_cmap('viridis', 100).colors
    # get the rgba of the color map that will be used
    red_map = mpl.cm.get_cmap('Reds')
    # assign new colors in the chosen color map
    newcolors[:5, :] = red_map(0.01)
    newcolors[5: 10, :] = red_map(0.3)
    newcolors[10: 20, :] = red_map(0.5)
    newcolors[20:50, :] = red_map(0.7)
    newcolors[50:100, :] = red_map(0.95)
    # create the customized color map
    mycmap = colors.ListedColormap(newcolors)
    # plot
    fig, ax = plt.subplots(figsize=(20, 4))
    sns.heatmap(
        data=data0,
        ax=ax,
        vmax=1, vmin=0,
        cmap=mycmap,
        cbar_kws={'pad': .02, 'ticks': [0, 0.05, 0.1, 0.2, 0.5, 1]},
        linewidths=0.01,
        linecolor='gray'
    )
    # set label
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
    # set title
    plt.text(25, 8, "A Heat Map for P-value", fontsize=20, fontstyle='italic')
    plt.show()


heat_map(p_matrix)