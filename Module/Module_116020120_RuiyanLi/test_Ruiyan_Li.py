from module_Ruiyan_Li import find_nth_frequent_words, panel_data_fill, calculate_VWAP
# A function to find the top n-th most frequent words in a text file
print(find_nth_frequent_words('news.txt', 5))

# A function to fill missing values in a panel data set
print(panel_data_fill('return.csv','Date','Security',['Return']))

# A function to calculate VWAP of stocks
calculate_VWAP('stock_sample_data.csv', 'Security', 'Adj Close', 'Volume')