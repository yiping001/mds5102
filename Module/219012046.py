#!/usr/bin/env python
# coding: utf-8


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os


st = "Module test"

def addone(x):
    x=x+1
    return(x)

class Foo:
    pass


def logit_transform(data):
    return np.log((data  1e-10).divide(1 - (data  1e-10)))

def ZNorm(x):    
    x = (x - np.mean(x)) / np.std(x)
    return x

def get_matching_pairs(treated_df, non_treated_df, col_i, scaler=True):
    treated_x = treated_df[col_i]
    non_treated_x = non_treated_df[col_i]
    
    if scaler == True:
        scaler = StandardScaler()
    if scaler:
        scaler.fit(treated_x)
        treated_x = scaler.transform(treated_x)
        non_treated_x = scaler.transform(non_treated_x)

    nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(non_treated_x)
    distances, indices = nbrs.kneighbors(treated_x)
    indices = indices.reshape(indices.shape[0])
    
    matched = non_treated_df.iloc[indices]
    return matched

def p_r_f1_a(acts,pres):
    TP, FP, TN, FN = 0, 0, 0, 0
    for i in range(len(acts)):
        if acts[i] == 1 and pres[i] == 1:
            TP = 1
        if acts[i] == 0 and pres[i] == 1:
            FP = 1
        if acts[i] == 1 and pres[i] == 0:
            FN = 1
        if acts[i] == 0 and pres[i] == 0:
            TN = 1    
    P = TP / (TPFP)
    R = TP / (TPFN)  
    # F1
    F1 = 2 / (1/P  1/R)
    A = (TPTN) / (TPFPFNTN) 
    print('Precision: {0:.3f}, Recall: {1:.3f}, F1:{2:.3f}, Accuracy:{3:.3f}'.format(P, R, F1, A))   
    return P, R, F1, A