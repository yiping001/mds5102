textUtils has 3 functions to process text
1. drawWordCloud() is to draw a word cloud plot for a text, input is a string variable
2. fog() is to calculate the gunning fog index of a text. The definition of gunning fog index is in https://en.wikipedia.org/wiki/Gunning_fog_index
3. readability() is to calculate the readability of a text, using textstat library, the definition of automated readability index is in https://en.wikipedia.org/wiki/Automated_readability_index

***************************************************************************
Before using textUtils, please install all the requirements using

pip install -r requirements.txt

***************************************************************************

