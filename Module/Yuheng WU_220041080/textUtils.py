
def drawWordCloud(text):
    from wordcloud import WordCloud, STOPWORDS
    import matplotlib.pyplot as plt
    from string import punctuation
    
    removing = punctuation + "，‘’“”。？！"
    
    #removing all the punctuations (including Chinese punctuations)
    for c in removing:
        text = text.replace(c," ")
   
    wc = WordCloud(width = 3000, height = 2000, random_state=1, background_color='black', colormap='Pastel1', collocations=False, stopwords = STOPWORDS).generate(text)
    plt.figure(figsize=(40,30))
    plt.imshow(wc)
    plt.axis("off")
    
    
#Calculate fog index, the definition of fog index is in https://en.wikipedia.org/wiki/Gunning_fog_index
def fog(text):
    from string import punctuation
    import syllables
    
    removing = punctuation + "，‘’“”。？！"
    sentences = text.split(".")
    
    for s in sentences:
        if s=='' or (s in removing):
            sentences.remove(s)
    
    #get a list or words(removing null string)
    for c in removing:
        text = text.replace(c," ")
    wordList = text.split(" ")
    while '' in wordList:
        wordList.remove('')
    
    complex_Num = 0
    for w in wordList:
        if syllables.estimate(w) >= 3:
            complex_Num = complex_Num + 1
    
    sent_Num = len(sentences)
    word_Num = len(wordList)
    
    fog = 0.4*(word_Num/sent_Num + 100 * complex_Num/word_Num)
    return fog


#automated readability index of the text, the definition of this index is in https://en.wikipedia.org/wiki/Automated_readability_index
def readability(text):
    import textstat as ts
    return ts.automated_readability_index(text)
