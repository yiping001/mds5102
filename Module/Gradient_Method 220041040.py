import numpy as np
import math
import matplotlib.pyplot as plt
import warnings



##########################################################
# function class
class fun():
    def __init__(self):
        pass

    def f1(self, x):
        return 3+x[0]+((1-x[1])*x[1]-2)*x[1]
    def f2(self, x):
        return 3+x[0]+(x[1]-3)*x[1]
# objective function
    def obj(self, x):
        return self.f1(x)**2 + self.f2(x)**2

    def g1(self, x):
        return np.array([1, 2*x[1]-3*x[1]**2-2])
    def g2(self, x):
        return np.array([1, 2*x[1]-3])
# gradient function
    def grad(self, x):
        return 2*self.f1(x)*self.g1(x) + 2*self.f2(x)*self.g2(x)
    def norm(self, x):
        return np.sqrt(np.sum(x ** 2))

fun = fun()


class SVM:

    def __init__(self,c,delta):
        self.c = c
        self.delta = delta

    def huber(self, x):
        delta = self.delta
        return 1/(2*delta) * max(0,x)**2 if x <= delta else x- delta/2

    def obj(self, x, y, feature,label):
        '''
        x,y are parameters of SVM hyperplane
        return the SVM objective function
        '''
        c = self.c
        sum = 0
        for i in range(len(feature)):
            sum += self.huber(1 - label[i]*(feature[i]*x + y))
        return (c/2) * norm(x,ord=2) + sum

    def grad_huber(self, x, y, feature, label):
        delta = self.delta
        gradsum = np.zeros(len(feature)+1)
        for i in range(len(x)):
            t = 1 - label[i] * (feature[i] * x + y)
            if t > delta:
                gradsum += np.ones(len(feature)+1)
            elif t>0 and t<= delta:
                gradsum += t*(np.append(label[i]*feature[i], label[i]))/delta
        return gradsum

    def grad_norm(self, x):
        c = self.c
        return np.append(c*x,0)

    def obj_grad(self,x, y, feature, label):
        c = self.c
        delta = self.delta
        return self.grad_norm(x,c)+ self.grad_huber(x, y, feature, label, delta)



############################################################
# parameter class
class parameter():
    def __init__(self):
        self.x_init = np.array([[-10,-2],[-10,-1],[-10,0],[-10,1],[-10,2],
                                [-5,-2],[-5,2],[5, -2],[5, 2],[0,-2],[0,2],
                                [10,-2],[10,-1],[10,0],[10,1],[10,2]]).astype(float)
        self.tol = 1e-5
        self.sigma = 0.5
        self.gamma = 0.1
        self.a = 2
        self.maxit = 100
        self.steptype = ['exact','diminishing','backtracking']
param = parameter()

############################################################
# exact line search with golden section

def golden_section(obj, x0, direction,tol):
    phi = (3-math.sqrt(5))/2
    x_l = x0
    x_r = x0 + param.a * direction
    for i in range(param.maxit):
        if fun.norm(x_l - x_r)<tol:
            break
        x_l_new = (1-phi)*x_l + phi*x_r
        x_r_new = (1-phi)*x_r + phi*x_l
        if obj(x_l_new) < obj(x_r_new):
            x_r = x_r_new
        else:
            x_l = x_l_new
    return (x_l + x_r)/2

# gradient method
def gradient_method(obj, grad, x0, tol, gamma, sigma,steptype):
    list_x = []
    list_x.append(x0)
    grad_list = []
    gradient = grad(x0)
    grad_list.append(fun.norm(gradient))
    k=0
    print('initial point :', list_x[0])
    while fun.norm(gradient)>= tol:

        direction = -gradient
        if steptype == 'exact':
            x0 = golden_section(obj, x0, direction, 1e-6)
        if steptype == 'backtracking':
            s = 1
            while(obj(x0 + s*direction)> obj(x0)-gamma*s*fun.norm(gradient)**2):
                s = sigma*s
            x0 = x0 + s*direction
        if steptype == 'diminishing':
            alpha = 0.01/math.log(k + 15)
            x0 = x0 + alpha*direction
        list_x.append(x0)
        gradient = grad(x0)
        grad_list.append(fun.norm(gradient))
        k += 1
        #print(k,x0)
    print('steps of {} :'.format(steptype), k)
    print('convergence point of {} :'.format(steptype), list_x[-1])
    return list_x, grad_list, k
##############################################################
# plot the picture of step methods

def plot_contour():
    X1 = np.arange(-15, 15, 0.05)
    X2 = np.linspace(-3,3,100)
    X1, X2 = np.meshgrid(X1, X2)
    Y = (3+X1+((1-X2)*X2-2)*X2)**2 + (3+X1+(X2-3)*X2)**2
    plt.contour(X1, X2, Y, 100, colors= 'grey')
    plt.text(-0.9, 0.9, '(-1, 1)')
    plt.text(-6.9, -1.1, '(-7, -1)')
    plt.text(-2.9, -0.1, '(-3, 0)')

def plot_path(x_path):
    x_path = np.array(x_path)
    plt.plot(x_path[:, 0], x_path[:, 1], linewidth = 1)
    plt.plot(x_path[:, 0], x_path[:, 1])


def plot_convergrnce(x_path):
    eps = 1e-8
    x_path = np.array(x_path)
    x_convergence = x_path- x_path[-1] + eps
    y = np.log(list(map(fun.norm,x_convergence)))
    x = np.arange(len(y))
    plt.plot(x, y)

def plot_gradient(x_gradient):
    x_gradient = np.array(x_gradient)
    y = np.log(x_gradient)
    plt.plot(y)


def plot1(x_init, steptypes):
    for steptype in steptypes:
        x_path , x_grad, k = gradient_method(fun.obj, fun.grad, x_init, param.tol, param.gamma,
                                 param.sigma, steptype)
        plt.figure(1)
        plot_convergrnce(x_path)
        plt.title('||x-x*|| of {}'.format(steptype))
        plt.figure(2)
        plot_gradient(x_grad)
        plt.title('gradient of {} :'.format(steptype))
        plt.show()


def plot2(x_init, steptypes):
    plot_contour()
    for steptype in steptypes:
        for x0 in x_init:
            x_path, x_grad, k = gradient_method(fun.obj, fun.grad, x0, param.tol, param.gamma,
                                        param.sigma, steptype)
            plot_path(x_path)
        plt.title('path of {}'.format(steptype))
        plt.show()
####################################################################
if __name__ == '__main__':

    # plot convergence and gradient of [0, 0]
    plot1(x_init= np.array([0,0]), steptypes= param.steptype)
    # plot x path of methods of initial points
    plot2(x_init= param.x_init, steptypes= param.steptype)


