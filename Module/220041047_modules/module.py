import math
import datetime
import pandas as pd
import requests


def word_counter(filename = "iso.txt"):
    words = []
    with open(filename) as infile:
        lines = infile.readlines()
        for line in lines:
            words += line.split()
    word_dict = {}
    for word in words:
        word_dict[word] = word_dict.get(word, 0) + 1

    tmp = [(value, key) for key, value in word_dict.items()]
    tmp = sorted(tmp, reverse=True)
    print("The most frequent TEN words are: ")
    for i in range(10):
        print("No.{} frequent word: {} : {} times".format(i+1,  tmp[i][1],  tmp[i][0])  )


def security_stat(filename="lecture5_sample_data.csv"):
    with open(filename) as infile:
        lines = infile.readlines()
    lines = [line.strip('\n').split(',')  for line in lines]

    sec_dict = {}
    for line in lines[1:]:
        sec_name = line[0]
        if sec_name in sec_dict:
            if line[6] != '' and float(line[6]) != 0.0:
                sec_dict[sec_name].append(float(line[6]))
        else:
            sec_dict[sec_name] = []
    sec_dict = {key:value for key, value in sec_dict.items() if len(value) != 0}
    
    sec_stat_dict = {}
    for key, value in sec_dict.items():
        sec_stat_dict[key] = sum(value) / len(value)    
    print("The mean values are: ", sec_stat_dict)

    sec_dev_dict = {}
    for key, value in sec_dict.items():
        dev = 0
        for price in value:
            dev += (price - sec_stat_dict[key]) ** 2
        dev = dev / len(value)   
        sec_dev_dict[key] = math.sqrt(dev)
    print("The stddev values are: ", sec_dev_dict)
    


def fetch_Yahoo_Finance(security):
    """
        Usage: load yahoo finance historical data pages
        Keyword: (requests module, pandas html reader)
    """
    yahoo_finance_url = 'https://au.finance.yahoo.com/quote/%s/history?period1=1546261200&period2=1559829600&interval=1d&filter=history&frequency=1d'
    yahoo_hist_price_page = requests.get(yahoo_finance_url % security)
    if yahoo_hist_price_page.status_code == 200:
        print('Successful Fetching:', security)
        return pd.read_html(yahoo_hist_price_page.text)[0]
    else:
        raise Exception('Some Error Happened. Http Error Code %d' % yahoo_hist_price_page.status_code)
















