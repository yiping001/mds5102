"""
    Module for the Python Programming
    Author: Chen GONG (220041030)
"""

import math
import requests
import datetime
import pandas as pd



# Lecture-01

def allowance_calculator(start_date="2020-9-1", end_date="2020-12-25", rate="36"):
    """
        Usage: calculate the total allowance within a time period
        Keyword: (string split, datetime module)
    """
    # Pre-processing
    start_date_data = [int(i) for i in start_date.split('-')]
    end_date_data = [int(i) for i in end_date.split('-')]

    # Read into datetime objects
    dt_start = datetime.datetime(*start_date_data)
    dt_end = datetime.datetime(*end_date_data)

    # Calculate and output
    num_days = (dt_end - dt_start).days
    allowance = int(rate) * num_days
    print("Total allowance from {} to {} is: {} RMB".format(start_date, end_date, allowance))
    



# Lecture-02

def weekly_income():
    """ 
        Usage: calculate the weekly income with the input of working hours from users
        Keyword: (input method, try-except structure, if-elif-else structure)
    """
    hours = input("How many hours do you work? ")
    try:
        hours = int(hours)
    except:
        hours = -1

    pay = 0
    if hours < 0:
        print("Please enter a number!")
    elif hours > 36:
        pay = 36 * 120 + (hours - 36) * 220
        print("Weekly pay is: " + str(pay))
    else:
        pay = hours * 120
        print("Weekly pay is: " + str(pay))
    

def get_vwap(stock_name='AAPL'):
    """
        Usage: calculate the VWAP from a csv file
        Keyword: (csv reader, pandas, try-except structure)
    """
    aapl = pd.read_csv('./%s.csv' % stock_name)
    col_prod_sum = (aapl['Close'] * aapl['Volume']).sum()
    col_sum = aapl['Volume'].sum()
    try:
        vwap = col_prod_sum / col_sum
    except ZeroDivisionError:
        vwap = -1
    return vwap



# Lecture-03

def word_frequency(text_name='./iso.txt'):
    """
        Usage: count the frequencies of words in a text file
        Keyword: (file reading, dictionary)
    """
    # Read the text file
    infile = open(text_name, 'r')
    lines = infile.readlines()
    infile.close()
    # Put words into a dictionary
    word_freq = {}
    for line in lines:
        for word in line.strip('\n').split():
            if word in word_freq:
                word_freq[word] += 1
            else:
                word_freq[word] = 1
    print(word_freq)
    # Find the most frequent one
    max_word = None
    max_freq = 0
    for key, val in word_freq.items():
        if max_word is None or val > max_freq:
            max_freq = val
            max_word = key    
    print("The most frequent word is {}, for {} times.".format(max_word, max_freq))


def zero_mover(alist):
    """
        Usage: move all the zeros in a list to the back, without changing the order of other numbers
        Keyword: (double pointers)
    """
    i = 0
    j = 0
    while j < len(alist):
        if alist[j] != 0:
            alist[i], alist[j] = alist[j], alist[i]
            i += 1
        j += 1
    print(alist)

            
            
# Lecture-04

def get_domain_name(mail_address):
    """
        Usage: get the domain name from an email address
        Keyword: (string split)
    """
    words = mail_address.split()
    domain_name = words[1].split('@')[1]
    print("The domain name is: --- {} ---".format(domain_name))


def sequence_mean():
    """
        Usage: calculate the mean for a sequence of user input
        Keyword: (list append/sum/len, while-input interaction)
    """
    print("Please enter a sequence of numbers. (Input a non-number word to end.)\n")
    num_list = []
    while True:
        num = input("Next number: ")
        try:
            num = int(num)
            num_list.append(num)
        except:
            break
        
    mean = sum(num_list) / len(num_list)
    print("The mean value of {} is: --- {} ---".format(num_list, mean))


def word_cleaner(input_info="Eeny, meeny, miny moe, Catch a tiger by the toe. If he squeals, let him go."):
    """
        Usage: convert words into lower cases and put into a set
        Keyword: (set comprehension)
    """
    word_list = input_info.split()
    word_set = {word.strip(',').strip('.').lower() for word in word_list}
    print(word_set)



# Lecture-05

def word_counter(filename = "./iso.txt"):
    """
        Usage: count the word frequencies in a text file and print the most frequent ten words
        Keyword: (file reading, dictionary, sorting)
    """
    # Read all the words into a list
    words = []
    with open(filename) as infile:
        lines = infile.readlines()
        for line in lines:
            words += line.split()

    # Put them into a dictionary
    word_dict = {}
    for word in words:
        word_dict[word] = word_dict.get(word, 0) + 1

    # Sort by value
    tmp = [(value, key) for key, value in word_dict.items()]
    tmp = sorted(tmp, reverse=True)

    # Print the result
    print("The most frequent TEN words are: ")
    for i in range(10):
        print("No.{} frequent word: --- {} ---: {} times".format(i+1,  tmp[i][1],  tmp[i][0])  )


def security_stat(filename="./lecture5_sample_data.csv"):
    """
        Usage: calculate the mean and standard deviation for each security
        Keyword: (dictionary)
    """
    with open(filename) as infile:
        lines = infile.readlines()
    lines = [line.strip('\n').split(',')  for line in lines]

    sec_dict = {}
    for line in lines[1:]:
        sec_name = line[0]
        if sec_name in sec_dict:
            if line[6] != '' and float(line[6]) != 0.0:
                sec_dict[sec_name].append(float(line[6]))
        else:
            sec_dict[sec_name] = []
    sec_dict = {key:value for key, value in sec_dict.items() if len(value) != 0}
    
    sec_stat_dict = {}
    for key, value in sec_dict.items():
        sec_stat_dict[key] = sum(value) / len(value)    
    print("The mean values are: ", sec_stat_dict)

    sec_dev_dict = {}
    for key, value in sec_dict.items():
        dev = 0
        for price in value:
            dev += (price - sec_stat_dict[key]) ** 2
        dev = dev / len(value)   
        sec_dev_dict[key] = math.sqrt(dev)
    print("The stddev values are: ", sec_dev_dict)
    


# Lecture-06

def fetch_Yahoo_Finance(security):
    """
        Usage: load yahoo finance historical data pages
        Keyword: (requests module, pandas html reader)
    """
    yahoo_finance_url = 'https://au.finance.yahoo.com/quote/%s/history?period1=1546261200&period2=1559829600&interval=1d&filter=history&frequency=1d'
    yahoo_hist_price_page = requests.get(yahoo_finance_url % security)
    if yahoo_hist_price_page.status_code == 200:
        print('Successful Fetching:', security)
        return pd.read_html(yahoo_hist_price_page.text)[0]
    else:
        raise Exception('Some Error Happened. Http Error Code %d' % yahoo_hist_price_page.status_code)




if __name__ == "__main__":
    allowance_calculator()
    weekly_income()
    zero_mover([9,3,5,0,2,9,0,1,0,25])
    get_domain_name("From firstname.surname@cuhk.edu.cn Tue Oct 06 10 9:17:00")
    sequence_mean()
    word_cleaner()
    fetch_Yahoo_Finance('AAPL')















