#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Match two stocks using propensity score method
def Match(groups, propensity, caliper = 0.01):
       
    N  = len(groups)
    # N1: number of Treat groups ; N2: number of Control groups
    N1 = int(groups.sum())
    N2 = N - N1
    g1 = propensity[groups == 1]
    g2 = propensity[groups == 0]
            
    # Randomly permute the smaller group to get order for matching
    morder     = np.random.permutation(g1.index)
    matches    = pd.Series(np.empty(N1))
    matches[:] = np.NAN
    
    for m in morder:
        dist   = abs(g1[m] - g2)
        if dist.min() <= caliper:
            matches[m] = dist.argmin()
            g2 = g2.drop(matches[m])
    
    return (matches)

# Normalize a column of data
def normalize(df,column): 
    mean = df[column].mean()
    std = df[column].std()
    df[column] = (df[column] - mean) / std
    return df

# Match two stocks using minimum distance method
def match_by_distance(df,columns,treated_col='treated'): 
    ### Normalization ### 
    threshold = 0.2
    dist_list = []
    for column in columns:
        df = normalize(df,column)

    print(df.head())

    df1 = df[df[treated_col] == 1]
    df2 = df[df[treated_col] == 0]
    df1['match'] = 0
    
    morder  = np.random.permutation(df1.index)

    for m in morder:
        dist = df2.copy()
        dist['distance'] = 0
        for column in columns:
            dist[column] -= df1[column][m]
            dist['distance'] += dist[column] ** 2
        idx = dist['distance'].idxmin()
        min_dist = dist['distance'][idx]
        dist_list.append(dist['distance'][idx])
        if min_dist < threshold:
            df1['match'][m] = df2['security'][idx]
            df2.drop(idx,inplace=True)
    #print(np.mean(dist_list))
    #print(dist_list)
    return df1

