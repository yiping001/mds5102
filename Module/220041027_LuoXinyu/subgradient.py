import numpy as np
from numpy import random
import pandas as pd
import time
import matplotlib.pyplot as plt
import math

def generate_initial_x(n):
    np.random.seed(0)
    initial_x = random.random(size=(n,1))
    return (initial_x)

def generate_func( n, X):
    np.random.seed(0)
    # generate function
    A = random.random(size=(n,n))
    b = random.random(size=(n,1))
    value = np.linalg.norm(np.dot(A,X)-b,ord =1)
    return (A,b,value)

def compute_subgradient(x, A, b):
    sign = []
    row, col = A.shape
    c = np.dot(A,x) - b
    for i in range(row):
        if c[i] > 0:
            sign.append(1)
        if c[i] < 0:
            sign.append(-1)
        if c[i] == 0:
            s = random.uniform(-1,1)
            sign.append(s)
    si = np.array(sign)
    si.shape = (row, 1)
    subgradient = np.dot(A.T,si)
    return (subgradient)

def fixed_step_subgradient_method(func,step,eps,initial_x):

    x = initial_x
    # initialization
    A, b, value = func
    values = [1]
    xs = []
    iterations = 0
    row, col = A.shape
    subgradient = 0
    tic = time.time()

    while True:
        # gradient update
        direction = -subgradient
        x = x + step*direction
        iterations = iterations+1

        value = np.linalg.norm(np.dot(A, x) - b, ord=1)
        subgradient = compute_subgradient(x, A, b)

        values.append(value)
        xs.append(x)

        if (values[iterations]-values[iterations-1])** 2 <= eps**2:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

def fixed_steplength_subgradient_method(func,steplength,eps,initial_x):

    x = initial_x
    # initialization
    A, b, value = func
    values = [1]
    xs = []
    iterations = 0
    row, col = A.shape
    subgradient = compute_subgradient(x, A, b)
    tic = time.time()

    while True:

        # gradient update
        direction = -subgradient
        step = steplength/np.linalg.norm(subgradient,ord =2)
        x = x + step*direction
        iterations = iterations+1

        value = np.linalg.norm(np.dot(A, x) - b, ord=1)
        subgradient = compute_subgradient(x, A, b)

        values.append(value)
        xs.append(x)

        if (values[iterations]-values[iterations-1])** 2 <= eps**2:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

def diminishing_step_subgradient_method(func,step,eps,initial_x):

    x = initial_x
    # initialization
    A, b, value = func
    values = [1]
    xs = []
    iterations = 0
    row, col = A.shape
    subgradient = compute_subgradient(x, A, b)
    tic = time.time()

    while True:

        # gradient update
        iterations = iterations + 1
        direction = -subgradient
        step = step/iterations
        x = x + step*direction

        value = np.linalg.norm(np.dot(A, x) - b, ord=1)
        subgradient = compute_subgradient(x, A, b)

        values.append(value)
        xs.append(x)

        if (values[iterations] - values[iterations-1])**2 <= eps * 100:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

def diminishing_sqrt_step_subgradient_method(func,step,eps,initial_x):

    x = initial_x
    # initialization
    A, b, value = func
    values = [1]
    xs = []
    iterations = 0
    row, col = A.shape
    subgradient = compute_subgradient(x, A, b)
    tic = time.time()

    while True:

        # gradient update
        iterations = iterations + 1
        direction = -subgradient
        step = step/math.sqrt(iterations)
        x = x + step*direction

        value = np.linalg.norm(np.dot(A, x) - b, ord=1)
        subgradient = compute_subgradient(x, A, b)

        values.append(value)
        xs.append(x)

        if (values[iterations] - 0)  <= eps*100:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)
n = 5
initial_x = generate_initial_x(n)
func = generate_func(n,initial_x)
step = 0.01
steplength = 0.01
eps  = 1e-5

# fixed_step_subgradient_method
step = 0.01
x, values, sub_xs, sub_tim, iterations = fixed_step_subgradient_method(func,step,eps,initial_x)
# fixed_steplength_subgradient_method
x_, values_, sub_xs, sub_tim, iterations_ = fixed_steplength_subgradient_method(func,step,eps,initial_x)

# plot
plt.plot(values[1:iterations_-1],label='stepsize = 0.01')
plt.plot(values_[1:iterations_-1],label='fixed_steplength = 0.01')
plt.xlabel('iterations')
plt.ylabel('the value of loss function')
plt.title('subgradient descent')
plt.legend()
plt.grid()
plt.show()



# diminishing_step_subgradient_method
x_1, values_1, sub_xs, sub_tim, iterations_ = diminishing_step_subgradient_method(func,step,eps,initial_x)
x_2, values_2, sub_xs, sub_tim, iterations = diminishing_sqrt_step_subgradient_method(func,step,eps,initial_x)

# plot
plt.plot(values_1[1:iterations_-1],label='stepsize = 0.01')
plt.plot(values_2[1:iterations_-1],label='fixed_steplength = 0.01')
plt.xlabel('iterations')
plt.ylabel('the value of loss function')
plt.title('subgradient_descent')
plt.legend()
plt.grid()
plt.show()

