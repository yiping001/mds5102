import numpy as np
import pandas as pd
from numpy import random
import time
import math
import matplotlib.pyplot as plt

'''
    # generate random sample without
    x_rand_array = np.arange(A.shape[0])
    y_rand_array = np.arange(b.shape[0])

    np.random.shuffle(x_rand_array)
    np.random.shuffle(y_rand_array)

    x_rand = array[x_rand_array[0:n]]
    y_rand = array[y_rand_array[0:n]]
'''

'''
矩阵运算相对于for循环运算的提速
'''
a = np.random.rand(100000)
b = np.random.rand(100000)

tic = time.time()
c = np.dot(a,b)
toc = time.time()
print(str(1000*(toc - tic)) + 'ms')


a = np.random.rand(100000)
b = np.random.rand(100000)
c = 0
tic = time.time()
for i in range(100000):
    c = c + a[i]*b[i]
toc = time.time()

print(str(1000*(toc - tic)) + 'ms')


def generate_func_gradient_step( n, m, X):
    np.random.seed(0)
    # generate function
    A = random.random(size=(m,n))
    b = random.random(size=(m,1))
    value = math.pow(np.linalg.norm(np.dot(A,X)-b,ord =2),2)
    # compute gradient
    gradient = 2* np.dot(np.dot(A.T,A),X)-2* np.dot(A.T,b)
    Hesse = 2* np.dot(A.T,A)
    # compute step with exact line search
    step = np.dot(gradient.T,gradient)/np.dot(np.dot(gradient.T,Hesse),gradient)
    return (A, b, value, gradient, step)

def generate_initial_x(n):
    np.random.seed(0)
    initial_x = random.random(size=(n,1))
    return (initial_x)

def opt_x(A,b):
    x_opt =np.dot(np.dot(np.linalg.pinv(np.dot(A.T,A)),A.T),b)
    return(x_opt)

def steepest_gradient(func ,eps,initial_x):
    '''
    :param func: generate_func_gradient_step( n, m, X)的返回值
    :param eps: 阈值
    :param initial_x: 初始点
    :return x, values, xs,x_opt, tim, iterations: 近似解, 函数值序列, 迭代点序列, 解析解, 耗时, 迭代次数
    '''
    if eps <= 0:
        raise ValueError("Epsilon must be positive")
    x = initial_x

    # initialization
    A, b, value, gradient, step = func
    values = []
    steps = []
    xs = []
    iterations = 0
    gradient = 0
    tic = time.time()

    while True:

        steps.append(step)
        values.append(value)
        xs.append(x)

        direction = -gradient
        x = x + step*direction
        iterations = iterations+1

        value = math.pow(np.linalg.norm(np.dot(A, x) - b, ord=2), 2)

        gradient = 2 * np.dot(np.dot(A.T, A), x) - 2 * np.dot(A.T, b)
        Hesse = 2 * np.dot(A.T, A)

        step = np.dot(np.dot(gradient.T, gradient), np.linalg.inv(np.dot(np.dot(gradient.T, Hesse), gradient)))

        if np.linalg.norm(gradient, ord = 2) ** 2 <= eps**2:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

def sdg_uniform_fixed_step(func,eps,initial_x,step):

    x = initial_x
    # initialization
    A, b, value, gradient, step_ = func
    values = []
    xs = []
    iterations = 0
    row, col = A.shape
    gradient = 0
    tic = time.time()

    while True:

        # random choice
        s = np.random.choice(row, size=1, replace=True)
        dA = A[s,]
        db = b[s,]

        # gradient update
        direction = -gradient
        x = x + step*direction
        iterations = iterations+1

        value = math.pow(np.linalg.norm(np.dot(dA, x) - db, ord=2), 2)
        gradient = 2 * np.dot(np.dot(dA.T, dA), x) - 2 * np.dot(dA.T, db)

        values.append(value)
        xs.append(x)

        if np.linalg.norm(gradient, ord = 2) ** 2 <= eps**2:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

# generate initial_x and function
n = 100
m = 80
initial_x = generate_initial_x(n)
func = generate_func_gradient_step(n,m,initial_x)
A, b, value, gradient, step = func
eps = 1e-5
step = 0.01
# steepest_gradient
x, values, xs, tim, iterations = steepest_gradient(func,eps,initial_x)
x_sgd, values_sgd, xs_sgd, tim_sgd, iterations_sgd = sdg_uniform_fixed_step(func, eps,initial_x,step)

plt.plot(values[iterations-100:iterations-1],label = 'exact line search')
plt.legend()
plt.show()

steps = [0.05,0.01,0.001]
values = pd.DataFrame(columns=('0.05', '0.01', '0.001'))
s = 0
for i in steps:
    cols = values.columns[s]
    step = i
    x_sgd, values_sgd, xs_sgd, tim_sgd, iterations_sgd = sdg_uniform_fixed_step(func, eps, initial_x, step)
    values[cols] = values_sgd[iterations_sgd-100:iterations_sgd-1]
    s = s+1

for i in range(3):
    plt_label = values.columns[i]
    plt.plot(values.iloc[:,i],label = 'step='+str(plt_label))
plt.legend()
plt.show()


