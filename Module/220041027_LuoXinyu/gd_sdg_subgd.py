import numpy as np
import pandas as pd
from numpy import random
import time
import math
import matplotlib.pyplot as plt

'''
generate function
'''
def generate_func_gradient_step( n, m, X):
    np.random.seed(0)
    # generate function
    A = random.random(size=(m,n))
    b = random.random(size=(m,1))
    value = math.pow(np.linalg.norm(np.dot(A,X)-b,ord =2),2)
    # compute gradient
    gradient = 2* np.dot(np.dot(A.T,A),X)-2* np.dot(A.T,b)
    Hesse = 2* np.dot(A.T,A)
    return (A, b, value, gradient)

def generate_initial_x(n):
    np.random.seed(0)
    initial_x = random.random(size=(n,1))
    return (initial_x)

def opt_x(A,b):
    x_opt =np.dot(np.dot(np.linalg.pinv(np.dot(A.T,A)),A.T),b)
    return(x_opt)

# generate the sample function and initial x
n = 20
m = 15
initial_x = generate_initial_x(n)
func = generate_func_gradient_step(n,m,initial_x)
A, b, value, gradient= func
eps = 1e-5


# full_batch_gradient_descent_fixed
def gradient_descent_fixed(func,step,eps,initial_x):
    if eps <= 0:
        raise ValueError("Epsilon must be positive")
    x = initial_x

    # initialization
    A, b, value, gradient= func
    values = []
    xs = []
    iterations = 0
    gradient = 0
    tic = time.time()

    while True:

        direction = - gradient
        x = x + step*direction
        iterations = iterations+1

        value = math.pow(np.linalg.norm(np.dot(A, x) - b, ord=2), 2)
        gradient = 2 * np.dot(np.dot(A.T, A), x) - 2 * np.dot(A.T, b)

        values.append(value)
        xs.append(x)

        if np.linalg.norm(gradient, ord = 2) ** 2 <= eps:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

# full_batch_gradient_descent_diminishing
def gradient_descent_diminishing(func,step,eps,initial_x):
    if eps <= 0:
        raise ValueError("Epsilon must be positive")
    x = initial_x

    # initialization
    A, b, value, gradient= func
    values = []
    steps = []
    xs = []
    iterations = 0
    gradient = 0
    tic = time.time()

    while True:

        values.append(value)
        xs.append(x)

        direction = -gradient
        iterations = iterations+1
        x = x + (step/iterations)*direction
        steps.append(step/iterations)

        value = math.pow(np.linalg.norm(np.dot(A, x) - b, ord=2), 2)

        gradient = 2 * np.dot(np.dot(A.T, A), x) - 2 * np.dot(A.T, b)
        Hesse = 2 * np.dot(A.T, A)

        if np.linalg.norm(gradient, ord = 2) ** 2 <= eps**2:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

step = 0.01
x, values, xs, tim, iterations = gradient_descent_fixed(func,step,eps,initial_x)
step = 0.001
x_, values_, xs, tim, iterations_ = gradient_descent_fixed(func,step,eps,initial_x)

# plot
plt.plot(values[50:iterations-1], label='stepsize = 0.01')
plt.plot(values_[50:iterations-1],label='stepsize = 0.001')
plt.xlabel('iterations')
plt.ylabel('the value of loss function')
plt.title('full batch gradient descent')
plt.legend()
plt.grid()
plt.show()

# stochastic_gradient_descent_fixed
def sdg_fixed_step(func,eps,initial_x,step):

    x = initial_x
    # initialization
    A, b, value, gradient = func
    values = []
    xs = []
    iterations = 0
    row, col = A.shape
    gradient = 0
    tic = time.time()
    while True:

        # random choice
        s = np.random.choice(row, size=1, replace=True)
        dA = A[s,]
        db = b[s,]

        # gradient update
        direction = -gradient
        x = x + step*direction
        iterations = iterations+1

        value = math.pow(np.linalg.norm(np.dot(dA, x) - db, ord=2), 2)
        gradient = 2 * np.dot(np.dot(dA.T, dA), x) - 2 * np.dot(dA.T, db)

        values.append(value)
        xs.append(x)

        if np.linalg.norm(gradient, ord = 2) ** 2 <= eps:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

# stochastic_gradient_descent_diminishing
def sdg_diminishing_step(func,eps,initial_x,step):

    x = initial_x
    # initialization
    A, b, value,gradient= func
    values = []
    steps = []
    xs = []
    iterations = 0
    row, col = A.shape
    gradient = 0
    tic = time.time()

    while True:

        # random choice
        s = np.random.choice(row, size=1, replace=True)
        dA = A[s,]
        db = b[s,]

        # gradient update
        direction = -gradient
        iterations = iterations+1
        x = x + (step/iterations)*direction

        value = math.pow(np.linalg.norm(np.dot(dA, x) - db, ord=2), 2)
        gradient = 2 * np.dot(np.dot(dA.T, dA), x) - 2 * np.dot(dA.T, db)

        steps.append(step)
        values.append(value)
        xs.append(x)

        if np.linalg.norm(gradient, ord = 2) ** 2 <= eps:
            break

    toc = time.time()
    tim = str(1000 * (toc - tic)) + 'ms'
    return (x, values, xs, tim, iterations)

step = 0.01
x_sgd, values_sgd, xs_sgd, tim_sgd, iterations_sgd = sdg_fixed_step(func, eps,initial_x,step)
x_sgd_, values_sgd_, xs_sgd, tim_sgd, iterations_sgd_ = sdg_fixed_step(func, eps,initial_x,step)

# plot
plt.plot(values_sgd_[50:iterations_sgd_-1],label='stepsize = 0.01/t')
plt.plot(values_sgd[50:iterations_sgd_-1], label='stepsize = 0.01')
plt.xlabel('iterations')
plt.ylabel('the value of loss function')
plt.title('stochastic gradient descent')
plt.legend()
plt.grid()
plt.show()