st = 'Successful Module Testing, MDS5102'
dt = [100, 200, 300]

def addone(x):
    x = x + 1
    return x

class Foo:
    pass

def word_count(file_name):
    words = []
    with open(file_name, 'r') as f:
        for line in f.readlines():
            # print(line)
            line = line.replace('\n', ' ')
            words.extend(line.split(' '))
    word_count = dict()
    for word in words:
        if word in word_count.keys():
            word_count[word] += 1
        else:
            word_count[word] = 1
    word_count.pop('')
    maxcount = None
    maxword = None
    for word, count in word_count.items():
        if maxcount is None or count > maxcount:
            maxword = word
            maxcount = count
    print(maxword, maxcount)
    return maxword, maxcount

def find_smallest(num_list):
    temp_smallest = None
    for number in num_list:
        if temp_smallest == None:
            temp_smallest = number
        elif number < temp_smallest:
            temp_smallest = number
        print(number, temp_smallest)
    print('After', temp_smallest)
    return temp_smallest